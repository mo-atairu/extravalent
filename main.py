import bottle
from search.searchapp import search
from search.contactapp import contact
from config.common import TEMPLATE_DIRS

bottle.TEMPLATE_PATH = TEMPLATE_DIRS

default_app = search.app
contact_app = contact.app

default_app.mount('/contact', contact_app)

# See gunicorn config
