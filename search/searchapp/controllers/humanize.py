from datetime import date, timedelta

def ending(time):

    try:
        if time == 'today':
            return date.today() + timedelta(days=0)
        if time == 'tomorrow':
            return date.today() + timedelta(days=1)
        if time == 'dayafter':
            return date.today() + timedelta(days=2)
        if time == 'yesterday':
            return date.today() + timedelta(days=-1)
        else:
            return date.today() + timedelta(days=0)
    except BaseException:
        return date.today() + timedelta(days=0)
