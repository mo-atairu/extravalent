def add_favorites(domain):
    s = list(exp.select(exp.fav).where(exp.domain == domain))[0].fav
    if s == 0:
        exp.update(fav=1).where(exp.domain == domain).execute()
        res = {"action": "added"}

    else:
        exp.update(fav=0).where(exp.domain == domain).execute()
        res = {"action": "removed"}

    return dumps(res)
