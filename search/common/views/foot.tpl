<footer class="sticky">Extravalent</footer>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.semanticui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.13/semantic.min.js"></script>


<script>$('#expired').DataTable({
        "lengthMenu":[200, 100, "All"],
        "searching":false,
        "lengthChange":true});
</script>

<script>
var app = angular.module('fav',[]);
app.controller('saveFave', function($scope, $http){
    $scope.dname = function(name){
            var cl = String(name.slice(-0,-4));
            $http.get("http://search.theperfectdomain.net/fav/" + name,{
                headers: {'Authorization': 'pa55w0rd'}
            })
            .then(function(response){
            res = response.data.action;
            if (res=='added'){
            angular.element('i.'+cl).css('visibility','visible');}
            else{angular.element('i.'+cl).css('visibility','hidden');}
            }

            );
    }

});
app.config(function($interpolateProvider){
$interpolateProvider.startSymbol('{$');
$interpolateProvider.endSymbol('$}');
});
</script>


</body>
</html>
